# Python OOP의 순간 (Part 1) 

## Python에서의 객체 지향 프로그래밍

![](./images/1_IIdkr4IJDS5Te_vOzTeSqQ.webp)

OOP 또는 객체 지향 프로그래밍은 개발자와 컴퓨터 프로그래머가 소프트웨어 설계를 기능과 로직이 아닌 소프트웨어에 사용되는 데이터 또는 객체를 중심으로 구성하는 데 사용하는 프로그래밍 모델이다.

데이터 또는 객체를 중심으로 프로그램을 설계하면 프로그래머는 재사용 가능하고 깔끔하고 구조화된 프로그램 및/또는 코드를 작성할 수 있다. 이를 통해 모든 팀원은 미리 정의된 구조를 따르는 구조화된 방식으로 팀 작업을 수행하여 전체 프로세스를 깔끔하고 잘 유지 관리할 수 있다.

Python 또는 기타 프로그래밍 언어에서 객체는 고유한 속성과 동작을 갖는 데이터 필드로 정의할 수 있다. 객체를 통해 프로그램은 프로그램 전체에서 사용되는 작업과 변수에 대한 사양을 이해할 수 있다.

OOP는 로직을 중심에 두기보다는 로직을 통해 조작해야 하는 객체에 초점을 맞춘다. 이 접근 방식은 프로그래머와 관리자 즉 팀원들이 모바일 어플리케이션이나 시스템 시뮬레이션 소프트웨어의 코드를 포함하여 크고 복잡하며 활발하게 업데이트되는 프로그램/코드를 관리하고 유지보수하는 데 도움이 된다.

OOP는 작업자/프로그래머가 코드를 유지 관리하고 동일한 프로젝트을 위한 협업 환경에서 작업하도록 도와준다.

또한 재사용성, 확장성 및 효율성이라는 추가적인 이점을 갖는다.

이 포스팅에서는 추상화, 캡슐화, 상속 및 다형성이라는 OOP의 네 가지 기둥에 대해 설명한다.

코드를 통해 객체 지향 프로그래밍의 네 가지 기둥을 설명한다.

## 추상화(Abstraction)

![](./images/1_09pNWIC_pd6nHrcQV17pOQ.webp)


### 추상 클래스(Abstract Class)란?
몸체가 없거나, 아무 작업도 수행하지 않거나, 논리를 가지고 있지 않은 메서드를 추상 메서드(abstarct method)라고 한다. 추상 메서드를 구성하는 클래스를 추상 클래스라고 한다. 그리고 추상 클래스는 객체를 만들 수 없다. 그러나 Python은 추상 클래스 생성을 지원하지 않는다. Python에서 추상 클래스를 만들려면 라이브러리 `abc`를 사용하고 여기에서 `ABC`와 `abstractmethod`를 임포트한다. 그런 다음 클래스 내부에 데코레이터 `@abstractmethod`를 사용하여 클래스를 추상 클래스로 정의해야 한다.

또한 추상 클래스를 상속하는 클래스는 부모 클래스에 존재하는 모든 추상 메서드를 정의해야 한다. 이것이 Python의 객체 지향 프로그래밍에서 추상화와 상속을 사용할 수 있는 방법이다.

### 왜 필요한가?
추상 클래스는 시스템을 개발하고 이를 위한 코드를 oops 메서드로 작성할 때 유용하다. 코드에 흐름과 체계적인 구조를 부여하여 코드를 깔끔하고 재사용 가능하게 만들 수 있다. 추상화는 코드에 흐름을 만들고 프로젝트를 위하여 깔끔하고 구조화된 코드를 만드는 방법이다. 여러 기능과 이를 위한 클래스가 있는 대규모 어플리케이션의 경우, 추상화를 따르고 추상화를 사용하는 연습은 프로그래머가 코드를 이해하기 쉬운 방식으로 설계하는 데 도움이 된다.

다음은 Python에서 추상화의 기본적인 예를 보여준다.

```python
from abc import ABC, abstractmethod


#defining an abstract class with one abstract method
class computer(ABC):
  @abstractmethod
  def process(self):
    pass

#defining a class inheriting the abstract class
class laptop(computer):
  def process(self):
    print("a working portable computer")

#creating an object of child class
lap = laptop()

#calling the abstract method defined in the class "laptop"
lap.process()
```

```
output:

a working portable computer
```

## 상속

![](./images/1_X3T8057ARkUQehQXIywtcQ.webp)

객체 지향 프로그래밍에서 상속은 클래스와 객체, 또는 여러 클래스와 객체 간의 관계를 연결하거나 설정하는 개념이다.

상속의 문자 그대로의 의미는 부모로부터 속성을 상속받는 것이다. OOP에서는 상속이라는 개념을 통해 부모 클래스의 속성을 자식 클래스에서 매우 쉽게 사용할 수 있다.

여기서 흥미로운 점은 부모 클래스로부터 속성과 메서드를 상속받는 것과 함께 자식 클래스도 자체 속성과 메서드를 가질 수 있다는 것이다. 또한 직접 상속과 간접 상속과 함께 다중 상속을 수행할 수도 있다.

부모 클래스에 정의된 속성, 객체 메서드를 자식 클래스를 통해 쉽게 사용할 수 있기 때문에 프로그래머는 깔끔하고 간결하며 연결된 코드를 작성할 수 있다.

다음은 Python에서 코드를 통한 상속의 예를 보여준다.

### 예: Human Body

```python
# defining the first class, the first parent class
class head:
  def head(self):
    print("At the top we have head. \n")

# inheriting the properties and methods form head into torso
class torso(head):
  def torso(self):
    print("Below head is the torso. The hands are attatched to the torso of a human body. \n")

# inheriting the properties and method from torso to seat while indirectly inheriting from head also
class seat(torso):
  def seat(self):
    print("Below the torso is the private section of a human body. The below belt body starts here. \n")

# inheriting from all head, torso, and seat indirectly and directly (from seat only)
class legs(seat):
  def legs(self):
    print("At the end, the human body has legs, which include the foot and toes. \n")

# inheriting from all head, torso, seat, and legs
class human_body(legs):
  def body(self):
    print("This is the complete human body!")

# creating an object of class human_body only
Human = human_body()

# class human_body possess all the methods from all the classes through direct
# and indirect inheritance

Human.head()
Human.torso()
Human.seat()
Human.legs()
Human.body()
```

```
Output:

At the top we have head. 

Below head is the torso. The hands are attatched to the torso of a human body. 

Below the torso is the private section of a human body. The below belt body starts here. 

At the end, the human body has legs, which include the foot and toes. 

This is the complete human body!
```

## 캡슐화(Encapsulation)

![](./images/1_iXZzfC16x9SGZ7mHvYQIOQ.webp)

'캡슐화'는 한 마디로 코드와 프로그램의 안전을 보장하기 위해 사용자로부터 무언가를 숨기는 것을 의미한다.

객체 지향 프로그래밍에서는 사용자가 실제 변수를 건드리지 않고 원하는 작업을 수행할 수 있도록 특정 메서드를 만들어 변수와 객체를 숨기거나 사용자가 직접 접근하지 못하도록 제한하는 개념이다.

캡슐화의 원칙은 모든 중요한 정보는 객체에 포함되어 있으며, 개발자는 가장 필요한 정보만 선택적으로 노출해야 한다는 것이다.

'캡슐화'라는 숨기기의 개념을 사용하면 코드의 보안이 강화되고 불필요하거나 의도하지 않은 데이터 손상을 방지할 수 있다.

실제 사용 사례 또는 업계 수준의 사용 사례에서 '캡슐화'는 회사의 소중한 자산을 보호하는 데 매우 중요하다.

다음은 Python에서 '캡슐화'를 수행하는 방법에 대한 코드를 보여준다.

### 예 01: 은행 구좌

```python
class bank_account:

  def __init__(self, name, age, typee, balance, interest):
    self.__name = name
    self.__age = age
    self.__typee = typee
    self.__balance = balance
    self.__interest = interest

  def credit(self, amount):
    if amount < 0:
      return False
    else:
      self.__balance = self.__balance + amount
      return True

  def withdraw(self, amount):
    if self.__balance >= amount:
      self.__balance = self.__balance - amount
      return True
    else:
      return False

  def get_balance(self):
    return self.__balance
```

```python
account01 = bank_account("Isha Choudhary", "24", "Savings", 15000, 3.25)

account01.get_balance()
```

```
Output:

15000
```

```python
account01.credit(-4253)
```

```
Output:

False
```

```python
account01.credit(5000)
```

```
Output:

True
```

```python
account01.get_balance()
```

```
Output:

20000
```

```python
account01.withdraw(200000)
```

```
Output:

False
```

```python
account01.withdraw(2000)
```

```
Output:

True
```

```python
account01.get_balance()
```

```
Output:

18000
```

### 예 02: Healthcare

```python
class healthcare:

  def __init__(self, name, age, weight, height, bp, sugar, history, current_disease):
    self.__name = name
    self.__age = age
    self.__weight = weight
    self.__height = height
    self.__bp = bp
    self.__sugar = sugar
    self.__history = history
    self.__current_disease = current_disease

  def intro_details(self):
    print("Name: ", self.__name)
    print("Age: ", self.__age)
    print("Weight: ", self.__weight)
    print("Height: ", self.__height)
    print("Blood Pressure: ", self.__bp)
    print("Sugar Level: ", self.__sugar )
    return

  def get_history(self):
    return self.__history

  def get_problem(self):
    return self.__current_disease
```

```python
patient01 = healthcare("Shina", "45", "79", "168", "120/90", "80/70", "heart patient", "Anxiety")
```

```python
patient01.intro_details()
```

```
Output:

Name:  Shina
Age:  45
Weight:  79
Height:  168
Blood Pressure:  120/90
Sugar Level:  80/70
```

```python
patient01.get_problem()
```

```
Output:

'Anxiety'
```

```python
patient01.get_history()
```

```
Output:

'heart disease'
```

## 다형성(Polymorphism)

![](./images/0_hnyuYCL9j02C1tv6.webp)

다형성은 객체 지향 프로그래밍의 네 가지 기둥 중 하나이다. 다형성은 객체가 서로 동작을 공유하지만 데이터를 다른 방식으로 처리할 수 있도록 설계하는 개념이다.

여기서 실생활과 연결되는 간단한 예로 인간을 들 수 있는데, 우리는 어디를 가든 항상 같은 사람이다. 그러나 우리의 행위과 행동은 상황에 따라 달라진다. 하나의 로직을 가진 함수를 가진 클래스가 다른 객체와 다른 데이터를 다른 방식으로 처리할 수 있는 것과 같은 이치이다.

다형성에서 프로그램은 부모 클래스로부터 객체를 실행할 때마다 어떤 의미나 용도가 필요한지 결정한다. 이는 새로운 가능성이 생길 때마다 코드를 복제할 필요성을 줄여주고 프로그래머가 깔끔한 코드를 유지하도록 도와준다.

또한 부모 클래스의 기능을 확장하는 자식 클래스가 생성되어 서로 다른 타입의 객체가 동일한 인터페이스를 통과하여 각기 다른 타입의 객체에 대해 각각 결과를 생성할 수 있다.

다음 코드에서는 '다형성'의 개념을 Python 코드 형식으로 설명하고 있다.

### 예 01: 클래스와 강의 계획

```python
# creating polymmorphism through class and objects

# defining class 01
class data_science:
  def syllabus(self):
    print("The Data Science Syllabus contains heavy Mathematics")

# defining class 02
class web_development:
  def syllabus(self):
    print("The Web Development Syllabus in heavy on programming concepts")

# creating the polymorphism concept as a class parcer
def class_parcer(class_obj):
  for i in class_obj :
    i.syllabus()

# creating objects from both the classes
data_science = data_science()
web_dev = web_development()

# one variable for multiple objects
class_obj = [data_science, web_dev]

# passing the variable through function
class_parcer(class_obj)
```

```
Output:

The Data Science Syllabus contains heavy Mathematics
The Web Development Syllabus in heavy on programming concepts
```

### 예 02: 교수와 학생

```python
# defining students class
class students:

  def __init__(self, name, standard):
    self.name = name
    self.standard = standard

  def details(self):
    print("The student details are as follows: ")
    print('Student Name: ', self.name)
    print("Studying in class: ", self.standard)
    print('\n')
    return self.name, self.standard

# defining teachers class
class teachers:

  def __init__(self, name, key_class):
    self.name =  name
    self.key_class = key_class

  def details(self):
    print("The teacher details are as follows: ")
    print('Teacher Name: ', self.name)
    print('Teaching Class: ', self.key_class)
    return self.name, self.key_class

# creating the polymorphism concept
def class_parcer(class_obj):
  for i in class_obj:
    i.details()

# creating objects
Shaan = students("Shaan Singh", "10 A")
Rina = teachers("Rina Shah", "12 B")

#creating common variable for all objects from different classes
class_obj = [Shaan, Rina]

# the final class parcer to perform the polymorphism
class_parcer(class_obj)
```

```
Output:

The student details are as follows: 
Student Name:  Shaan Singh
Studying in class:  10 A


The teacher details are as follows: 
Teacher Name:  Rina Shah
Teaching Class:  12 B
```


